﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Tank2 : MonoBehaviour
{

    void Update()
    {

        // 左に移動
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Rotate(new Vector3(0.0f, -1.0f, 0.0f));
        }
        // 右に移動
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f));
        }
        // 前に移動
        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.Translate(0.0f, 0.0f, 0.125f);
        }
        // 後ろに移動
        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.Translate(0.0f, 0.0f, -0.125f);
        }
    }
}
