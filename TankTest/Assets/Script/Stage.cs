﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage : MonoBehaviour
{
    public GameObject effectPrefab;
    public AudioClip exprosionSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            // ぶつかってきたオブジェクトを破壊する
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Bullet2")
        {
            // ぶつかってきたオブジェクトを破壊する
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "EnemyBullet")
        {         
            // ぶつかってきたオブジェクトを破壊する
            Destroy(other.gameObject);
        }
    }

}
