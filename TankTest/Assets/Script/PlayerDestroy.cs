﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDestroy : MonoBehaviour
{
    public GameObject effectPrefab1;
    public GameObject effectPrefab2;
    public int tankHP;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            // HPを１ずつ減少させる。
            tankHP -= 1;

            // ぶつかってきた敵の弾を破壊する。
            Destroy(other.gameObject);

            if (tankHP > 0)
            {
                GameObject effect1 = Instantiate(effectPrefab1, transform.position, Quaternion.identity);
                Destroy(effect1, 1.0f);
            }
            else
            {
                // ぶつかってきたオブジェクトを破壊する
                Destroy(other.gameObject);

                //ゲームオーバーシーンに移動
                Invoke("GameOver", 0.01f);
            }
        }
    }

    void GameOver()
    {
        SceneManager.LoadScene("GameOverScene");
    }
}*/