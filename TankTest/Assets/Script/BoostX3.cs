using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostX3 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(40, 30, 0), ForceMode.VelocityChange);
    }
}
