﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBox : MonoBehaviour
{
    public GameObject effectPrefab;
    public int objectHP;
    public GameObject[] itemPrefabs;
    public AudioClip exprosionSound;

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet" || other.gameObject.tag == "EnemyBullet" || other.gameObject.tag == "Bullet2")
        {
            objectHP -= 1;
            AudioSource.PlayClipAtPoint(exprosionSound, transform.position);

            if (objectHP > 0)
            {
                Destroy(other.gameObject);
                GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 2.0f);
            }
            else
            {
                Destroy(other.gameObject);
                Destroy(this.gameObject);

                GameObject dropItem = itemPrefabs[Random.Range(0, itemPrefabs.Length)];

                Vector3 pos = transform.position;

                Instantiate(dropItem, transform.position, Quaternion.identity);
            }
        }
    }
}