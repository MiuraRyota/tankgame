﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Destroy : MonoBehaviour
{
    public GameObject effectPrefab;
    public int enemyHP;
    public Text HP;
    public AudioClip exprosionSound;

    void Start()
    {
        HP.text = "E HP:" + enemyHP;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        //プレイヤーの弾にぶつかったら
        if (other.CompareTag("Bullet"))
        {
            //体力を減らす
            enemyHP--;
            HP.text = "E HP:" + enemyHP;

            AudioSource.PlayClipAtPoint(exprosionSound, transform.position);
            //体力が残ってたら
            if (enemyHP > 0)
            {
               
                GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
                Destroy(effect, 2.0f);

                //ぶつかったプレイヤーの弾を破壊
                Destroy(other.gameObject);
            }
            else
            {
                // ぶつかってきたオブジェクトを破壊する
                Destroy(other.gameObject);

                //クリアシーンに移動
                Invoke("GameClear", 0.01f);
            }
        }
    }

    void GameClear()
    {
        SceneManager.LoadScene("ClearScene");
    }


    public void attack(int a)
    {
        enemyHP -= a;
        if(enemyHP<=0)
        {
            enemyHP = 0;
            HP.text = "E HP:" + enemyHP;
        }
        HP.text = "E HP:" + enemyHP;

        if(enemyHP<=0)
        {
            //クリアシーンに移動
            Invoke("GameClear", 0.01f);
        }
    }
}