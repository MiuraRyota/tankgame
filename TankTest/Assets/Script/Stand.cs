using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stand : MonoBehaviour
{
    public GameObject effectPrefab;
    public AudioClip exprosionSound;
    public int standHP;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            standHP -= 1;
            if (standHP < 0)
            {
                Destroy(this.gameObject);
            }
            else
            {
                // ぶつかってきたオブジェクトを破壊する
                Destroy(other.gameObject);
            }
        }

        if (other.gameObject.tag == "Bullet2")
        {
            standHP -= 1;
            if (standHP < 0)
            {
                Destroy(this.gameObject);
            }
            else
            {
                // ぶつかってきたオブジェクトを破壊する
                Destroy(other.gameObject);
            }
        }

        if(other.gameObject.tag == "Stage")
        {
            GameObject effect1 = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }

    }

}
