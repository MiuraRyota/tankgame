using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player1HP : MonoBehaviour
{
    public GameObject effectPrefab;
    public int playerHP;
    public Text HP;
    public AudioClip exprosionSound;
    // Start is called before the first frame update
    void Start()
    {
        HP.text = "HP:" + playerHP;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = gameObject.transform.position;
        if (pos.y < -10)
        {
            //ゲームオーバーシーンに移動
            Invoke("Win", 0.01f);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        //敵の弾に当たったら
        if (other.gameObject.tag == "Bullet2")
        {
            //体力を減らす
            playerHP -= 1;

            //残り体力を表示
            HP.text = "HP:" + playerHP;

            AudioSource.PlayClipAtPoint(exprosionSound, transform.position);
            //ぶつかった敵の弾を消す
            Destroy(other.gameObject);

            if (playerHP > 0)
            {
                GameObject effect1 = Instantiate(effectPrefab, transform.position, Quaternion.identity);
                Destroy(effect1, 1.0f);
            }
            else
            {
                // ぶつかってきたオブジェクトを破壊する
                Destroy(other.gameObject);

                //ゲームオーバーシーンに移動
                Invoke("Win", 0.01f);
            }
        }
    }


    void Win()
    {
        SceneManager.LoadScene("P2WinScene");
    }

    public void AddHP(int amount)
    {
        playerHP += amount;

        HP.text = "HP:" + playerHP;
    }

    public void attack(int a)
    {
        playerHP -= a;
        if (playerHP <= 0)
        {
            playerHP = 0;
            HP.text = "HP:" + playerHP;
        }
        HP.text = "HP:" + playerHP;

        if (playerHP <= 0)
        {
            //クリアシーンに移動
            Invoke("Win", 0.01f);
        }
    }
}
