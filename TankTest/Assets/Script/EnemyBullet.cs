﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public GameObject enemyShellPrefab;
    public float shotSpeed;
    public AudioClip shotSound;
    private int shotIntarval;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        shotIntarval += 1;

        //一定時間ごとに弾を発射させる
        if (shotIntarval % 60 == 0)
        {
            GameObject enemyShell = Instantiate(enemyShellPrefab, transform.position, Quaternion.identity);

            Rigidbody enemyShellRb = enemyShell.GetComponent<Rigidbody>();

            enemyShellRb.AddForce(transform.forward * shotSpeed);

            // 弾を10秒後に破壊する。(ぶつかったときに消えなかった場合に消す)
            Destroy(enemyShell, 5.0f);

            AudioSource.PlayClipAtPoint(shotSound, transform.position);
        }
    }

}