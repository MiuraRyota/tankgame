﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recovery : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject effectPrefab;
    private PlayerHP hp1;
    private Player2HP hp2;
    private Destroy ds;
    private int reward = 0;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player1")
        {
            reward = (int)Random.Range(1.0f, 10.0f);
            hp1 = GameObject.Find("Tank").GetComponent<PlayerHP>();

            hp1.AddHP(reward);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 1.0f);
        }

        if (other.gameObject.tag == "Player2")
        {
            reward = (int)Random.Range(1.0f, 10.0f);
            hp2 = GameObject.Find("Enemy").GetComponent<Player2HP>();

            hp2.AddHP(reward);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 1.0f);
        }

        if (other.gameObject.tag == "Player")
        {
            reward = (int)Random.Range(1.0f, 15.0f);
            hp1 = GameObject.Find("Tank").GetComponent<PlayerHP>();

            hp1.AddHP(reward);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 1.0f);
        }
    }
}
