﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject effectPrefab;
    private Destroy   des;
    private PlayerHP  hp;
    private Player1HP hp1;
    private Player2HP hp2;
    private int a = 0;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player1")
        {
            a = (int)Random.Range(1.0f, 5.0f);
            hp2 = GameObject.Find("Enemy").GetComponent<Player2HP>();

            hp2.attack(a);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 2.0f);
        }

        if (other.gameObject.tag == "Player2")
        {
            a = (int)Random.Range(1.0f, 5.0f);
            hp = GameObject.Find("Tank").GetComponent<PlayerHP>();

            hp.attack(a);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 2.0f);
        }

        if (other.gameObject.tag == "Player")
        {
            a = (int)Random.Range(1.0f, 20.0f);
            des = GameObject.Find("Enemy").GetComponent<Destroy>();

            des.attack(a);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 2.0f);
        }

        if (other.gameObject.tag == "Player2")
        {
            a = (int)Random.Range(1.0f, 5.0f);
            hp1 = GameObject.Find("Tank").GetComponent<Player1HP>();

            hp1.attack(a);

            Destroy(gameObject);
            GameObject effect = Instantiate(effectPrefab, transform.position, Quaternion.identity);
            Destroy(effect, 2.0f);
        }
    }
}
